# Code Diem Development Environment SetUp

This script is written for Ubuntu 16.04 and I will upgrade it to the next Ubuntu LTS.

## What does this script install?

This script installs:

- Beyond Compare
- gdebi
- GIMP & Inkscape & ImageMagick
- Google Chrome
- Nautilus Actions
- Skype
- Slack
- VLC
- VVV & dependencies (git, vagrant, virtual box)

It also generates a SSH key pair.

This is the base development environment I use at Code Diem.

## Is there anything else?

Yes. This script also creates some utility scripts at /usr/local/bin:

- new_wp: a script to easily add new WordPress projects to VVV using my own vvv-init.sh

## How do I use it?

```
git clone git@gitlab.com:borjalofe/codediem-dev-env.git ubuntu-dev-install
sudo chmod 755 env_setup
sudo ./env_setup
```
